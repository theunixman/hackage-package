Source: haskell-dav
Maintainer: Debian Haskell Group <pkg-haskell-maintainers@lists.alioth.debian.org>
Uploaders:
 Clint Adams <clint@debian.org>,
Priority: extra
Section: haskell
Build-Depends:
 cdbs,
 debhelper (>= 9),
 ghc (>= 8),
 ghc-ghci,
 ghc-prof,
 haskell-devscripts (>= 0.13),
 libghc-case-insensitive-dev (>= 0.4),
 libghc-case-insensitive-prof,
 libghc-either-dev (>= 4.3),
 libghc-either-prof,
 libghc-exceptions-dev,
 libghc-exceptions-prof,
 libghc-http-client-dev (>= 0.2),
 libghc-http-client-prof,
 libghc-http-client-tls-dev (>= 0.2),
 libghc-http-client-tls-prof,
 libghc-http-types-dev (>= 0.7),
 libghc-http-types-prof,
 libghc-lens-dev (>= 3.0),
 libghc-lens-prof,
 libghc-mtl-dev (>= 2.2.1),
 libghc-mtl-prof,
 libghc-network-dev (>= 2.6),
 libghc-network-prof,
 libghc-network-uri-dev (>= 2.6),
 libghc-network-uri-prof,
 libghc-optparse-applicative-dev (>= 0.10.0),
 libghc-optparse-applicative-prof,
 libghc-transformers-base-dev,
 libghc-transformers-base-prof,
 libghc-transformers-compat-dev (>= 0.3),
 libghc-transformers-compat-prof,
 libghc-utf8-string-dev,
 libghc-utf8-string-prof,
 libghc-xml-conduit-dev (>= 1.0),
 libghc-xml-conduit-prof,
 libghc-xml-hamlet-dev (<< 0.5),
 libghc-xml-hamlet-dev (>= 0.4),
 libghc-xml-hamlet-prof,
Build-Depends-Indep:
 ghc-doc,
 libghc-case-insensitive-doc,
 libghc-either-doc,
 libghc-exceptions-doc,
 libghc-http-client-doc,
 libghc-http-client-tls-doc,
 libghc-http-types-doc,
 libghc-lens-doc,
 libghc-mtl-doc,
 libghc-network-doc,
 libghc-network-uri-doc,
 libghc-optparse-applicative-doc,
 libghc-transformers-base-doc,
 libghc-transformers-compat-doc,
 libghc-utf8-string-doc,
 libghc-xml-conduit-doc,
 libghc-xml-hamlet-doc,
Standards-Version: 3.9.8
Homepage: http://floss.scru.org/hDAV
Vcs-Browser: https://anonscm.debian.org/cgit/pkg-haskell/DHG_packages.git/tree/p/haskell-dav
Vcs-Git: https://anonscm.debian.org/git/pkg-haskell/DHG_packages.git
X-Description: RFC 4918 WebDAV support
 This is a library for the Web Distributed Authoring and Versioning
 (WebDAV) extensions to HTTP.  At present it supports a very small
 subset of client functionality.

Package: hdav
Architecture: any
Section: web
Depends:
 ${haskell:Depends},
 ${misc:Depends},
 ${shlibs:Depends},
Description: command-line WebDAV client
 hdav currently only supports copying a file and associated WebDAV
 properties from one URL to another.

Package: libghc-dav-dev
Architecture: any
Depends:
 ${haskell:Depends},
 ${misc:Depends},
 ${shlibs:Depends},
Recommends:
 ${haskell:Recommends},
Suggests:
 ${haskell:Suggests},
Provides:
 ${haskell:Provides},
Description: ${haskell:ShortDescription}${haskell:ShortBlurb}
 ${haskell:LongDescription}
 .
 ${haskell:Blurb}

Package: libghc-dav-prof
Architecture: any
Depends:
 ${haskell:Depends},
 ${misc:Depends},
Recommends:
 ${haskell:Recommends},
Suggests:
 ${haskell:Suggests},
Provides:
 ${haskell:Provides},
Description: ${haskell:ShortDescription}${haskell:ShortBlurb}
 ${haskell:LongDescription}
 .
 ${haskell:Blurb}

Package: libghc-dav-doc
Architecture: all
Section: doc
Depends:
 ${haskell:Depends},
 ${misc:Depends},
Recommends:
 ${haskell:Recommends},
Suggests:
 ${haskell:Suggests},
Description: ${haskell:ShortDescription}${haskell:ShortBlurb}
 ${haskell:LongDescription}
 .
 ${haskell:Blurb}
