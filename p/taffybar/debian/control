Source: taffybar
Maintainer: Debian Haskell Group <pkg-haskell-maintainers@lists.alioth.debian.org>
Uploaders:
 Louis Bettens <louis@bettens.info>,
Priority: extra
Section: haskell
Build-Depends:
 cdbs,
 debhelper (>= 9),
 ghc (>= 8),
 ghc-prof,
 haskell-devscripts (>= 0.13),
 libghc-cairo-dev,
 libghc-cairo-prof,
 libghc-containers-dev,
 libghc-containers-prof,
 libghc-dbus-dev (<< 1.0),
 libghc-dbus-dev (>= 0.10.1),
 libghc-dbus-prof,
 libghc-dyre-dev (<< 0.9),
 libghc-dyre-dev (>= 0.8.6),
 libghc-dyre-prof,
 libghc-enclosed-exceptions-dev (>= 1.0.0.1),
 libghc-enclosed-exceptions-prof,
 libghc-filepath-dev,
 libghc-filepath-prof,
 libghc-gtk-dev (<< 0.15),
 libghc-gtk-dev (>= 0.12),
 libghc-gtk-dev (>= 0.12.1),
 libghc-gtk-prof,
 libghc-gtk-traymanager-dev (<< 0.2),
 libghc-gtk-traymanager-dev (>= 0.1.2),
 libghc-gtk-traymanager-prof,
 libghc-hstringtemplate-dev (<< 0.9),
 libghc-hstringtemplate-dev (>= 0.8),
 libghc-hstringtemplate-prof,
 libghc-http-dev,
 libghc-http-prof,
 libghc-mtl-dev (>= 2),
 libghc-mtl-prof,
 libghc-network-dev (<< 3),
 libghc-network-dev (>= 2.6),
 libghc-network-prof,
 libghc-network-uri-dev (<< 3),
 libghc-network-uri-dev (>= 2.6),
 libghc-network-uri-prof,
 libghc-old-locale-dev,
 libghc-old-locale-prof,
 libghc-parsec3-dev (>= 3.1),
 libghc-parsec3-prof,
 libghc-safe-dev (<< 1),
 libghc-safe-dev (>= 0.3),
 libghc-safe-prof,
 libghc-split-dev (>= 0.1.4.2),
 libghc-split-prof,
 libghc-stm-dev,
 libghc-stm-prof,
 libghc-text-dev,
 libghc-text-prof,
 libghc-time-locale-compat-dev (<< 0.2),
 libghc-time-locale-compat-dev (>= 0.1),
 libghc-time-locale-compat-prof,
 libghc-utf8-string-dev,
 libghc-utf8-string-prof,
 libghc-x11-dev (>= 1.5.0.1),
 libghc-x11-prof,
 libghc-xdg-basedir-dev,
 libghc-xdg-basedir-dev (<< 0.3),
 libghc-xdg-basedir-dev (>= 0.2),
 libghc-xdg-basedir-prof,
 libghc-xmonad-contrib-dev,
 libghc-xmonad-contrib-prof,
 libghc-xmonad-dev,
 libghc-xmonad-prof,
 libgtk2.0-dev,
Build-Depends-Indep:
 ghc-doc,
 libghc-cairo-doc,
 libghc-containers-doc,
 libghc-dbus-doc,
 libghc-dyre-doc,
 libghc-enclosed-exceptions-doc,
 libghc-filepath-doc,
 libghc-gtk-doc,
 libghc-gtk-traymanager-doc,
 libghc-hstringtemplate-doc,
 libghc-http-doc,
 libghc-mtl-doc,
 libghc-network-doc,
 libghc-network-uri-doc,
 libghc-old-locale-doc,
 libghc-parsec3-doc,
 libghc-safe-doc,
 libghc-split-doc,
 libghc-stm-doc,
 libghc-text-doc,
 libghc-time-locale-compat-doc,
 libghc-utf8-string-doc,
 libghc-x11-doc,
 libghc-xdg-basedir-doc,
 libghc-xmonad-contrib-doc,
 libghc-xmonad-doc,
Standards-Version: 3.9.8
Homepage: http://github.com/travitch/taffybar
Vcs-Browser: https://anonscm.debian.org/cgit/pkg-haskell/DHG_packages.git/tree/p/taffybar
Vcs-Git: https://anonscm.debian.org/git/pkg-haskell/DHG_packages.git
X-Description: desktop bar extensible in Haskell
 This bar is based on gtk2hs and provides several widgets (including a few
 graphical ones).
 It also sports an optional snazzy system tray.

Package: libghc-taffybar-dev
Architecture: any
Depends:
 libgtk2.0-dev,
 ${haskell:Depends},
 ${misc:Depends},
 ${shlibs:Depends},
Recommends:
 ${haskell:Recommends},
Suggests:
 ${haskell:Suggests},
Provides:
 ${haskell:Provides},
Description: ${haskell:ShortDescription}${haskell:ShortBlurb}
 ${haskell:LongDescription}
 .
 ${haskell:Blurb}

Package: libghc-taffybar-prof
Architecture: any
Depends:
 ${haskell:Depends},
 ${misc:Depends},
Recommends:
 ${haskell:Recommends},
Suggests:
 ${haskell:Suggests},
Provides:
 ${haskell:Provides},
Description: ${haskell:ShortDescription}${haskell:ShortBlurb}
 ${haskell:LongDescription}
 .
 ${haskell:Blurb}

Package: libghc-taffybar-doc
Architecture: all
Section: doc
Depends:
 ${haskell:Depends},
 ${misc:Depends},
Recommends:
 ${haskell:Recommends},
Suggests:
 ${haskell:Suggests},
Description: ${haskell:ShortDescription}${haskell:ShortBlurb}
 ${haskell:LongDescription}
 .
 ${haskell:Blurb}

Package: taffybar
Architecture: any
Depends:
 ${haskell:Depends},
 ${misc:Depends},
 ${shlibs:Depends},
Recommends:
 libghc-taffybar-dev,
 libghc-taffybar-doc,
 ${haskell:Recommends},
Suggests:
 ${haskell:Suggests},
Provides:
 ${haskell:Provides},
Description: desktop bar extensible in Haskell
 This bar is based on gtk2hs and provides several widgets (including a few
 graphical ones).
 It also sports an optional snazzy system tray.
 .
 This package comes pre-configured with the default configuration. If you want
 to build your custom-configured version, make sure that libghc-taffybar-dev is
 installed and put your configuration in ~/.config/taffybar/taffybar.hs
