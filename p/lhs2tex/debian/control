Source: lhs2tex
Maintainer: Debian Haskell Group <pkg-haskell-maintainers@lists.alioth.debian.org>
Uploaders:
 Arjan Oosting <arjan@debian.org>,
 Erik de Castro Lopo <erikd@mega-nerd.com>,
Priority: optional
Section: haskell
Build-Depends:
 autotools-dev,
 debhelper (>= 9),
 ghc (>= 7.6),
 libghc-regex-compat-dev,
 tex-common,
Standards-Version: 3.9.8
Homepage: http://people.cs.uu.nl/andres/lhs2tex/
Vcs-Browser: https://anonscm.debian.org/cgit/pkg-haskell/DHG_packages.git/tree/p/lhs2tex
Vcs-Git: https://anonscm.debian.org/git/pkg-haskell/DHG_packages.git

Package: lhs2tex
Architecture: any
Multi-Arch: foreign
Depends:
 texlive-latex-base,
 texlive-math-extra,
 ${misc:Depends},
 ${shlibs:Depends},
Description: Generates LaTeX code from literate Haskell sources
 lhs2TeX includes the following features:
 .
 Different styles to process your source file: for instance,
 "tt" style uses a monospaced font for the code while still
 allowing you to highlight keywords etc, whereas
 "poly" style uses proportional fonts for identifiers, handles
 indentation nicely, is able to replace binary operators by
 mathematical symbols and take care of complex horizontal
 alignments.
 .
 Formatting directives, which let you customize the way certain
 tokens in the source code should appear in the processed
 output.
 .
 A liberal parser that can handle most of the language
 extensions; you don't have to restrict yourself to Haskell 98.
 .
 Preprocessor-style conditionals that allow you to generate
 different versions of a document from a single source file
 (for instance, a paper and a presentation).
 .
 Active documents: you can use Haskell to generate parts of the
 document (useful for papers on Haskell).
 .
 A manual explaining all the important aspects of lhs2TeX.
