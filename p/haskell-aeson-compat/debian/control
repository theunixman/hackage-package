Source: haskell-aeson-compat
Maintainer: Debian Haskell Group <pkg-haskell-maintainers@lists.alioth.debian.org>
Uploaders:
 Clint Adams <clint@debian.org>,
Priority: extra
Section: haskell
Build-Depends:
 cdbs,
 debhelper (>= 9),
 ghc (>= 8),
 ghc-prof,
 haskell-devscripts (>= 0.13),
 libghc-aeson-dev (<< 1.1),
 libghc-aeson-dev (>= 0.7.0.6),
 libghc-aeson-prof,
 libghc-attoparsec-dev (<< 0.14),
 libghc-attoparsec-dev (>= 0.12),
 libghc-attoparsec-prof,
 libghc-base-compat-dev (<< 0.10),
 libghc-base-compat-dev (>= 0.6),
 libghc-base-compat-prof,
 libghc-base-orphans-dev (<< 0.6),
 libghc-base-orphans-dev (>= 0.4.5),
 libghc-exceptions-dev (<< 0.9),
 libghc-exceptions-dev (>= 0.8),
 libghc-exceptions-prof (<< 0.9),
 libghc-exceptions-prof (>= 0.8),
 libghc-hashable-dev (<< 1.3),
 libghc-hashable-dev (>= 1.2),
 libghc-hashable-prof,
 libghc-quickcheck-instances-dev (<< 0.3.13),
 libghc-quickcheck-instances-dev (>= 0.3),
 libghc-quickcheck2-dev (<< 2.9.2),
 libghc-quickcheck2-dev (>= 2.7.6),
 libghc-scientific-dev (<< 0.4),
 libghc-scientific-dev (>= 0.3),
 libghc-scientific-prof,
 libghc-semigroups-dev (<< 0.19),
 libghc-semigroups-dev (>= 0.16.2.2),
 libghc-semigroups-prof,
 libghc-tagged-dev (<< 0.9),
 libghc-tagged-dev (>= 0.7.3),
 libghc-tagged-prof,
 libghc-tasty-dev (<< 0.12),
 libghc-tasty-dev (>= 0.10),
 libghc-tasty-hunit-dev (<< 0.10),
 libghc-tasty-hunit-dev (>= 0.9),
 libghc-tasty-quickcheck-dev (<< 0.9),
 libghc-tasty-quickcheck-dev (>= 0.8),
 libghc-text-dev (<< 1.3),
 libghc-text-dev (>= 1.2),
 libghc-text-prof,
 libghc-time-locale-compat-dev (<< 0.2),
 libghc-time-locale-compat-dev (>= 0.1.0.1),
 libghc-time-locale-compat-prof,
 libghc-unordered-containers-dev (<< 0.3),
 libghc-unordered-containers-dev (>= 0.2),
 libghc-unordered-containers-prof (<< 0.3),
 libghc-unordered-containers-prof (>= 0.2),
 libghc-vector-dev (<< 0.12),
 libghc-vector-dev (>= 0.10),
 libghc-vector-prof,
Build-Depends-Indep:
 ghc-doc,
 libghc-aeson-doc,
 libghc-attoparsec-doc,
 libghc-base-compat-doc,
 libghc-exceptions-doc,
 libghc-hashable-doc,
 libghc-scientific-doc,
 libghc-semigroups-doc,
 libghc-tagged-doc,
 libghc-text-doc,
 libghc-time-locale-compat-doc,
 libghc-unordered-containers-doc,
 libghc-vector-doc,
Standards-Version: 3.9.8
Homepage: https://github.com/phadej/aeson-compat#readme
Vcs-Git: https://anonscm.debian.org/git/pkg-haskell/DHG_packages.git
Vcs-Browser: https://anonscm.debian.org/cgit/pkg-haskell/DHG_packages.git/tree/p/haskell-aeson-compat
X-Description: Compatibility layer for aeson
 Compatibility layer for aeson
  * decode etc. work as in aeson >=0.9
  * but it is generalised to work in any MonadThrow (that is extra)
  * .:? works as in aeson <0.10
  * .:! works as .:? in aeson ==0.10
  * Orphan instances FromJSON Day and FromJSON LocalTime for aeson <0.10

Package: libghc-aeson-compat-dev
Architecture: any
Depends:
 ${haskell:Depends},
 ${misc:Depends},
 ${shlibs:Depends},
Recommends:
 ${haskell:Recommends},
Suggests:
 ${haskell:Suggests},
Conflicts:
 ${haskell:Conflicts},
Provides:
 ${haskell:Provides},
Description: ${haskell:ShortDescription}${haskell:ShortBlurb}
 ${haskell:LongDescription}
 .
 ${haskell:Blurb}

Package: libghc-aeson-compat-prof
Architecture: any
Depends:
 ${haskell:Depends},
 ${misc:Depends},
Recommends:
 ${haskell:Recommends},
Suggests:
 ${haskell:Suggests},
Conflicts:
 ${haskell:Conflicts},
Provides:
 ${haskell:Provides},
Description: ${haskell:ShortDescription}${haskell:ShortBlurb}
 ${haskell:LongDescription}
 .
 ${haskell:Blurb}

Package: libghc-aeson-compat-doc
Architecture: all
Section: doc
Depends:
 ${haskell:Depends},
 ${misc:Depends},
Recommends:
 ${haskell:Recommends},
Suggests:
 ${haskell:Suggests},
Conflicts:
 ${haskell:Conflicts},
Description: ${haskell:ShortDescription}${haskell:ShortBlurb}
 ${haskell:LongDescription}
 .
 ${haskell:Blurb}
