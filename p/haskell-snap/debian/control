Source: haskell-snap
Maintainer: Debian Haskell Group <pkg-haskell-maintainers@lists.alioth.debian.org>
Uploaders:
 Clint Adams <clint@debian.org>,
Priority: extra
Section: haskell
Build-Depends:
 cdbs,
 debhelper (>= 9),
 ghc (>= 8),
 ghc-ghci,
 ghc-prof,
 haskell-devscripts (>= 0.13),
 libghc-aeson-dev (<< 1.1),
 libghc-aeson-dev (>= 0.6),
 libghc-aeson-prof,
 libghc-attoparsec-dev (<< 0.14),
 libghc-attoparsec-dev (>= 0.10),
 libghc-attoparsec-prof,
 libghc-cereal-dev (<< 0.6),
 libghc-cereal-dev (>= 0.3),
 libghc-cereal-prof,
 libghc-clientsession-dev (<< 0.10),
 libghc-clientsession-dev (>= 0.8),
 libghc-clientsession-prof,
 libghc-configurator-dev (<< 0.4),
 libghc-configurator-dev (>= 0.1),
 libghc-configurator-prof,
 libghc-directory-tree-dev (<< 0.13),
 libghc-directory-tree-dev (>= 0.10),
 libghc-directory-tree-prof,
 libghc-dlist-dev (<< 0.9),
 libghc-dlist-dev (>= 0.5),
 libghc-dlist-prof,
 libghc-hashable-dev (<< 1.3),
 libghc-hashable-dev (>= 1.2.0.6),
 libghc-hashable-prof,
 libghc-heist-dev (<< 1.1),
 libghc-heist-dev (>= 1.0),
 libghc-heist-prof,
 libghc-lens-dev (<< 4.15),
 libghc-lens-dev (>= 3.7.6),
 libghc-lens-prof,
 libghc-lifted-base-dev (<< 0.3),
 libghc-lifted-base-dev (>= 0.2),
 libghc-lifted-base-prof,
 libghc-map-syntax-dev (<< 0.3),
 libghc-map-syntax-dev (>= 0.2),
 libghc-map-syntax-prof,
 libghc-monad-control-dev (<< 1.1),
 libghc-monad-control-dev (>= 0.3),
 libghc-monad-control-prof,
 libghc-mtl-dev (<< 2.3),
 libghc-mtl-dev (>= 2.0),
 libghc-mtl-prof,
 libghc-mwc-random-dev (<< 0.14),
 libghc-mwc-random-dev (>= 0.8),
 libghc-mwc-random-prof,
 libghc-pwstore-fast-dev (<< 2.5),
 libghc-pwstore-fast-dev (>= 2.2),
 libghc-pwstore-fast-prof,
 libghc-snap-core-dev (<< 1.1),
 libghc-snap-core-dev (>= 1.0),
 libghc-snap-core-prof,
 libghc-snap-server-dev (<< 1.1),
 libghc-snap-server-dev (>= 1.0),
 libghc-snap-server-prof,
 libghc-stm-dev (<< 2.5),
 libghc-stm-dev (>= 2.2),
 libghc-stm-prof,
 libghc-text-dev (<< 1.3),
 libghc-text-dev (>= 0.11),
 libghc-text-prof,
 libghc-transformers-base-dev (<< 0.5),
 libghc-transformers-base-dev (>= 0.4),
 libghc-transformers-base-prof,
 libghc-unordered-containers-dev (<< 0.3),
 libghc-unordered-containers-dev (>= 0.1.4),
 libghc-unordered-containers-prof,
 libghc-xmlhtml-dev (<< 0.3),
 libghc-xmlhtml-dev (>= 0.1),
 libghc-xmlhtml-prof,
Build-Depends-Indep:
 ghc-doc,
 libghc-aeson-doc,
 libghc-attoparsec-doc,
 libghc-cereal-doc,
 libghc-clientsession-doc,
 libghc-configurator-doc,
 libghc-directory-tree-doc,
 libghc-dlist-doc,
 libghc-hashable-doc,
 libghc-heist-doc,
 libghc-lens-doc,
 libghc-lifted-base-doc,
 libghc-map-syntax-doc,
 libghc-monad-control-doc,
 libghc-mtl-doc,
 libghc-mwc-random-doc,
 libghc-pwstore-fast-doc,
 libghc-snap-core-doc,
 libghc-snap-server-doc,
 libghc-stm-doc,
 libghc-text-doc,
 libghc-transformers-base-doc,
 libghc-unordered-containers-doc,
 libghc-xmlhtml-doc,
Standards-Version: 3.9.8
Homepage: http://snapframework.com/
Vcs-Browser: https://anonscm.debian.org/cgit/pkg-haskell/DHG_packages.git/tree/p/haskell-snap
Vcs-Git: https://anonscm.debian.org/cgit/pkg-haskell/DHG_packages.git
X-Description: Snap Web Framework
 This is the top-level package for the official Snap Framework
 libraries. It includes:
 .
  * The Snaplets API
 .
  * Snaplets for sessions, authentication, and templates

Package: libghc-snap-dev
Architecture: any
Depends:
 ${haskell:Depends},
 ${misc:Depends},
 ${shlibs:Depends},
Recommends:
 ${haskell:Recommends},
Suggests:
 ${haskell:Suggests},
Provides:
 ${haskell:Provides},
Description: ${haskell:ShortDescription}${haskell:ShortBlurb}
 ${haskell:LongDescription}
 .
 ${haskell:Blurb}

Package: libghc-snap-prof
Architecture: any
Depends:
 ${haskell:Depends},
 ${misc:Depends},
Recommends:
 ${haskell:Recommends},
Suggests:
 ${haskell:Suggests},
Provides:
 ${haskell:Provides},
Description: ${haskell:ShortDescription}${haskell:ShortBlurb}
 ${haskell:LongDescription}
 .
 ${haskell:Blurb}

Package: libghc-snap-doc
Architecture: all
Section: doc
Depends:
 ${haskell:Depends},
 ${misc:Depends},
Recommends:
 ${haskell:Recommends},
Suggests:
 ${haskell:Suggests},
Description: ${haskell:ShortDescription}${haskell:ShortBlurb}
 ${haskell:LongDescription}
 .
 ${haskell:Blurb}
