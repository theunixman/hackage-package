Source: mighttpd2
Maintainer: Debian Haskell Group <pkg-haskell-maintainers@lists.alioth.debian.org>
Uploaders:
 Clint Adams <clint@debian.org>,
Priority: extra
Section: web
Build-Depends:
 cdbs,
 debhelper (>= 9),
 ghc (>= 8),
 ghc-prof,
 haskell-devscripts (>= 0.13),
 libghc-async-dev,
 libghc-auto-update-dev,
 libghc-blaze-builder-dev,
 libghc-byteorder-dev,
 libghc-case-insensitive-dev,
 libghc-conduit-dev (>= 1.1),
 libghc-conduit-extra-dev,
 libghc-hspec-dev (>= 1.3),
 libghc-http-client-dev,
 libghc-http-date-dev,
 libghc-http-types-dev,
 libghc-io-choice-dev,
 libghc-network-dev,
 libghc-old-locale-dev,
 libghc-parsec3-dev (>= 3),
 libghc-resourcet-dev,
 libghc-streaming-commons-dev,
 libghc-time-dev,
 libghc-tls-dev,
 libghc-transformers-dev,
 libghc-unix-time-dev,
 libghc-unordered-containers-dev,
 libghc-unordered-containers-prof,
 libghc-wai-app-file-cgi-dev (<< 3.2),
 libghc-wai-app-file-cgi-dev (>= 3.1),
 libghc-wai-app-file-cgi-prof,
 libghc-wai-dev (<< 3.3),
 libghc-wai-dev (>= 3.2),
 libghc-wai-logger-dev (>= 2.2.2),
 libghc-wai-prof,
 libghc-warp-dev (<< 3.3),
 libghc-warp-dev (>= 3.0.11),
 libghc-warp-dev (>= 3.2),
 libghc-warp-prof,
 libghc-warp-tls-dev (>= 3.0.1),
Standards-Version: 3.9.8
Homepage: http://hackage.haskell.org/package/mighttpd2
Vcs-Browser: https://anonscm.debian.org/cgit/pkg-haskell/DHG_packages.git/tree/p/mighttpd2
Vcs-Git: https://anonscm.debian.org/git/pkg-haskell/DHG_packages.git

Package: mighttpd2
Architecture: any
Depends:
 ${misc:Depends},
 ${shlibs:Depends},
Description: classical web server on WAI/warp
 Mighttpd2 (pronounced as "mighty") is a simple but practical HTTP server
 written in Haskell. It handles static files and CGI scripts. It also
 provides reverse proxy functionality.
 .
 Mighttpd2 is now implemented as a WAI application using the high-performance
 HTTP engine, "warp". To httperf Ping-Pong benchmark, Mighttpd2 is faster than
 nginx.
