Source: haskell-statistics
Maintainer: Debian Haskell Group <pkg-haskell-maintainers@lists.alioth.debian.org>
Uploaders:
 Joachim Breitner <nomeata@debian.org>,
Priority: extra
Section: haskell
Build-Depends:
 cdbs,
 debhelper (>= 9),
 ghc (>= 8),
 ghc-prof,
 haskell-devscripts (>= 0.13),
 libghc-aeson-dev (>= 0.6.0.0),
 libghc-aeson-prof,
 libghc-erf-dev,
 libghc-erf-prof,
 libghc-math-functions-dev (>= 0.1.5.2),
 libghc-math-functions-prof,
 libghc-monad-par-dev (>= 0.3.4),
 libghc-monad-par-prof,
 libghc-mwc-random-dev (>= 0.13.0.0),
 libghc-mwc-random-prof,
 libghc-primitive-dev (>= 0.3),
 libghc-primitive-prof,
 libghc-vector-algorithms-dev (>= 0.4),
 libghc-vector-algorithms-prof,
 libghc-vector-binary-instances-dev (>= 0.2.1),
 libghc-vector-binary-instances-prof,
 libghc-vector-dev (>= 0.10),
 libghc-vector-prof,
Build-Depends-Indep:
 ghc-doc,
 libghc-aeson-doc,
 libghc-erf-doc,
 libghc-math-functions-doc,
 libghc-monad-par-doc,
 libghc-mwc-random-doc,
 libghc-primitive-doc,
 libghc-vector-algorithms-doc,
 libghc-vector-binary-instances-doc,
 libghc-vector-doc,
Standards-Version: 3.9.8
Homepage: https://github.com/bos/statistics
Vcs-Browser: https://anonscm.debian.org/cgit/pkg-haskell/DHG_packages.git/tree/p/haskell-statistics
Vcs-Git: https://anonscm.debian.org/git/pkg-haskell/DHG_packages.git

Package: libghc-statistics-dev
Architecture: any
Depends:
 ${haskell:Depends},
 ${misc:Depends},
 ${shlibs:Depends},
Recommends:
 ${haskell:Recommends},
Suggests:
 ${haskell:Suggests},
Provides:
 ${haskell:Provides},
Description: A library of statistical types, data, and functions${haskell:ShortBlurb}
 This library provides a number of common functions and types useful
 in statistics.  Our focus is on high performance, numerical
 robustness, and use of good algorithms.  Where possible, we provide
 references to the statistical literature.
 .
 The library's facilities can be divided into three broad categories:
 .
 Working with widely used discrete and continuous probability
 distributions.  (There are dozens of exotic distributions in use; we
 focus on the most common.)
 .
 Computing with sample data: quantile estimation, kernel density
 estimation, bootstrap methods, regression and autocorrelation analysis.
 .
 Random variate generation under several different distributions.
 .
 ${haskell:Blurb}

Package: libghc-statistics-prof
Architecture: any
Depends:
 ${haskell:Depends},
 ${misc:Depends},
 ${shlibs:Depends},
Recommends:
 ${haskell:Recommends},
Suggests:
 ${haskell:Suggests},
Provides:
 ${haskell:Provides},
Description: A library of statistical types, data, and functions${haskell:ShortBlurb}
 This library provides a number of common functions and types useful
 in statistics.  Our focus is on high performance, numerical
 robustness, and use of good algorithms.  Where possible, we provide
 references to the statistical literature.
 .
 The library's facilities can be divided into three broad categories:
 .
 Working with widely used discrete and continuous probability
 distributions.  (There are dozens of exotic distributions in use; we
 focus on the most common.)
 .
 Computing with sample data: quantile estimation, kernel density
 estimation, bootstrap methods, and autocorrelation analysis.
 .
 Random variate generation under several different distributions.
 .
 ${haskell:Blurb}

Package: libghc-statistics-doc
Architecture: all
Section: doc
Depends:
 ${haskell:Depends},
 ${misc:Depends},
Recommends:
 ${haskell:Recommends},
Suggests:
 ${haskell:Suggests},
Description: A library of statistical types, data, and functions${haskell:ShortBlurb}
 This library provides a number of common functions and types useful
 in statistics.  Our focus is on high performance, numerical
 robustness, and use of good algorithms.  Where possible, we provide
 references to the statistical literature.
 .
 The library's facilities can be divided into three broad categories:
 .
 Working with widely used discrete and continuous probability
 distributions.  (There are dozens of exotic distributions in use; we
 focus on the most common.)
 .
 Computing with sample data: quantile estimation, kernel density
 estimation, bootstrap methods, and autocorrelation analysis.
 .
 Random variate generation under several different distributions.
 .
 ${haskell:Blurb}
