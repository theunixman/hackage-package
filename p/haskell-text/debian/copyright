Format: http://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Source: http://hackage.haskell.org/package/text

Files: *
Copyright: © 2008-2009 Tom Harper <rtharper@aftereternity.co.uk>
           © 2009-2011 Bryan O'Sullivan <bos@serpentine.com>
           © 2008-2009 Duncan Coutts <duncan@haskell.org>
           © 2009 Simon Marlow
           © 2008 Roman Leshchinskiy
	   © 2010 Johan Tibell
	   © 2011 MailRank, Inc
	   © 2008-2010 Björn Höhrmann <bjoern@hoehrmann.de>.
License: BSD
 All rights reserved.
 .
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions
 are met:
 .
     * Redistributions of source code must retain the above copyright
       notice, this list of conditions and the following disclaimer.
 .
     * Redistributions in binary form must reproduce the above
       copyright notice, this list of conditions and the following
       disclaimer in the documentation and/or other materials provided
       with the distribution.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

Files: scripts/CaseFolding.txt, scripts/SpecialCasing.txt
Copyright: © 1991-2009 Unicode, Inc.
License: other
 All rights reserved. Distributed under the Terms of Use in
 http://www.unicode.org/copyright.html.
 .
 Permission is hereby granted, free of charge, to any person obtaining a
 copy of the Unicode data files and any associated documentation (the "Data
 Files") or Unicode software and any associated documentation (the "Software")
 to deal in the Data Files or Software without restriction, including without
 limitation the rights to use, copy, modify, merge, publish, distribute,
 and/or sell copies of the Data Files or Software, and to permit persons to
 whom the Data Files or Software are furnished to do so, provided that (a) the
 above copyright notice(s) and this permission notice appear with all copies
 of the Data Files or Software, (b) both the above copyright notice(s) and
 this permission notice appear in associated documentation, and (c) there
 is clear notice in each modified Data File or in the Software as well as in
 the documentation associated with the Data File(s) or Software that the data
 or software has been modified.
 .
 THE DATA FILES AND SOFTWARE ARE PROVIDED "AS IS", WITHOUT WARRANTY OF ANY
 KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT OF
 THIRD PARTY RIGHTS. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR HOLDERS
 INCLUDED IN THIS NOTICE BE LIABLE FOR ANY CLAIM, OR ANY SPECIAL INDIRECT OR
 CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE,
 DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
 TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE
 OF THE DATA FILES OR SOFTWARE.
 .
 Except as contained in this notice, the name of a copyright holder shall
 not be used in advertising or otherwise to promote the sale, use or other
 dealings in these Data Files or Software without prior written authorization
 of the copyright holder.

Files: debian/*
Copyright: © 2009 Giovanni Mascellani <gio@debian.org>
License: GPL-3+
 The Debian packaging information is under the GPL, version 3 or later.  On
 Debian GNU/Linux systems, the complete text of the GNU General Public License
 Version 3 can be found in `/usr/share/common-licenses/GPL-3'.
