Source: haskell-safesemaphore
Maintainer: Debian Haskell Group <pkg-haskell-maintainers@lists.alioth.debian.org>
Uploaders:
 Joey Hess <joeyh@debian.org>,
Priority: extra
Section: haskell
Build-Depends:
 cdbs,
 debhelper (>= 9),
 ghc (>= 8),
 ghc-prof,
 haskell-devscripts (>= 0.13),
 libghc-stm-dev,
 libghc-stm-prof,
Build-Depends-Indep:
 ghc-doc,
 libghc-stm-doc,
Standards-Version: 3.9.8
Homepage: https://github.com/ChrisKuklewicz/SafeSemaphore
Vcs-Browser: https://anonscm.debian.org/cgit/pkg-haskell/DHG_packages.git/tree/p/haskell-safesemaphore
Vcs-Git: https://anonscm.debian.org/git/pkg-haskell/DHG_packages.git

Package: libghc-safesemaphore-dev
Architecture: any
Depends:
 ${haskell:Depends},
 ${misc:Depends},
 ${shlibs:Depends},
Recommends:
 ${haskell:Recommends},
Suggests:
 ${haskell:Suggests},
Provides:
 ${haskell:Provides},
Description: exception safe alternatives to QSem, QSemN, and SampleVar - GHC libraries${haskell:ShortBlurb}
 This is an alternative to the deprecated QSem, QSemN, and SampleVar in
 GHC base. Those base modules are not exception safe and can be broken
 by killThread.
 .
 ${haskell:Blurb}

Package: libghc-safesemaphore-prof
Architecture: any
Depends:
 ${haskell:Depends},
 ${misc:Depends},
Recommends:
 ${haskell:Recommends},
Suggests:
 ${haskell:Suggests},
Provides:
 ${haskell:Provides},
Description: exception safe alternatives to QSem, QSemN, and SampleVar - GHC profiling data${haskell:ShortBlurb}
 This is an alternative to the deprecated QSem, QSemN, and SampleVar in
 GHC base. Those base modules are not exception safe and can be broken
 by killThread.
 .
 ${haskell:Blurb}

Package: libghc-safesemaphore-doc
Architecture: all
Section: doc
Depends:
 ${haskell:Depends},
 ${misc:Depends},
Recommends:
 ${haskell:Recommends},
Suggests:
 ${haskell:Suggests},
Description: exception safe alternatives to QSem, QSemN, and SampleVar - documentation${haskell:ShortBlurb}
 This is an alternative to the deprecated QSem, QSemN, and SampleVar in
 GHC base. Those base modules are not exception safe and can be broken
 by killThread.
 .
 ${haskell:Blurb}
