Source: haskell-type-level-numbers
Maintainer: Debian Haskell Group <pkg-haskell-maintainers@lists.alioth.debian.org>
Uploaders: Sean Whitton <spwhitton@spwhitton.name>
Priority: extra
Section: haskell
Build-Depends:
 cdbs,
 debhelper (>= 9),
 ghc (>= 8),
 ghc-prof,
 haskell-devscripts (>= 0.13),
Build-Depends-Indep: ghc-doc
Standards-Version: 3.9.8
Homepage: http://hackage.haskell.org/package/type-level-numbers
Vcs-Browser: https://anonscm.debian.org/cgit/pkg-haskell/DHG_packages.git/tree/p/haskell-type-level-numbers
Vcs-Git: https://anonscm.debian.org/cgit/pkg-haskell/DHG_packages.git
X-Description: library representing integers using Haskell type families
 This library represents numbers in Haskell at the level of types.
 This is achieved by means of type families.
 .
 Numbers up to 2^18-1 can be represented.
 .
 In this version of the package, comparison of numbers, subtraction
 and multiplication of numbers is supported.

Package: libghc-type-level-numbers-dev
Architecture: any
Depends: ${haskell:Depends}, ${misc:Depends}, ${shlibs:Depends}
Recommends: ${haskell:Recommends}
Suggests: ${haskell:Suggests}
Conflicts: ${haskell:Conflicts}
Provides: ${haskell:Provides}
Description: ${haskell:ShortDescription}${haskell:ShortBlurb}
 ${haskell:LongDescription}
 .
 ${haskell:Blurb}

Package: libghc-type-level-numbers-prof
Architecture: any
Depends: ${haskell:Depends}, ${misc:Depends}
Recommends: ${haskell:Recommends}
Suggests: ${haskell:Suggests}
Conflicts: ${haskell:Conflicts}
Provides: ${haskell:Provides}
Description: ${haskell:ShortDescription}${haskell:ShortBlurb}
 ${haskell:LongDescription}
 .
 ${haskell:Blurb}

Package: libghc-type-level-numbers-doc
Architecture: all
Section: doc
Depends: ${haskell:Depends}, ${misc:Depends}
Recommends: ${haskell:Recommends}
Suggests: ${haskell:Suggests}
Conflicts: ${haskell:Conflicts}
Description: ${haskell:ShortDescription}${haskell:ShortBlurb}
 ${haskell:LongDescription}
 .
 ${haskell:Blurb}
