Source: haskell-conduit-extra
Maintainer: Debian Haskell Group <pkg-haskell-maintainers@lists.alioth.debian.org>
Uploaders: Clint Adams <clint@debian.org>
Priority: extra
Section: haskell
Build-Depends:
 cdbs,
 debhelper (>= 9),
 ghc (>= 8),
 ghc-prof,
 haskell-devscripts (>= 0.13),
 libghc-async-dev,
 libghc-async-prof,
 libghc-attoparsec-dev (>= 0.10),
 libghc-attoparsec-prof,
 libghc-blaze-builder-dev (>= 0.3),
 libghc-blaze-builder-prof,
 libghc-conduit-dev (<< 1.3),
 libghc-conduit-dev (>= 1.1),
 libghc-conduit-prof,
 libghc-exceptions-dev,
 libghc-exceptions-prof,
 libghc-hspec-dev (>= 1.3),
 libghc-monad-control-dev,
 libghc-monad-control-prof,
 libghc-network-dev (>= 2.3),
 libghc-network-prof,
 libghc-primitive-dev (>= 0.5),
 libghc-primitive-prof,
 libghc-quickcheck2-dev,
 libghc-resourcet-dev (>= 1.1),
 libghc-resourcet-prof,
 libghc-stm-dev,
 libghc-stm-prof,
 libghc-streaming-commons-dev (>= 0.1.16),
 libghc-streaming-commons-prof,
 libghc-text-dev,
 libghc-text-prof,
 libghc-transformers-base-dev,
 libghc-transformers-base-prof,
Build-Depends-Indep:
 ghc-doc,
 libghc-async-doc,
 libghc-attoparsec-doc,
 libghc-blaze-builder-doc,
 libghc-conduit-doc,
 libghc-exceptions-doc,
 libghc-monad-control-doc,
 libghc-network-doc,
 libghc-primitive-doc,
 libghc-resourcet-doc,
 libghc-stm-doc,
 libghc-streaming-commons-doc,
 libghc-text-doc,
 libghc-transformers-base-doc,
Standards-Version: 3.9.8
Homepage: http://github.com/snoyberg/conduit
Vcs-Browser: https://anonscm.debian.org/cgit/pkg-haskell/DHG_packages.git/tree/p/haskell-conduit-extra
Vcs-Git: https://anonscm.debian.org/git/pkg-haskell/DHG_packages.git
X-Description: batteries-included conduit: adapters for common libraries
 The conduit package itself maintains relative small dependencies. The
 purpose of this package is to collect commonly used utility functions
 wrapping other library dependencies, without depending on
 heavier-weight dependencies. The basic idea is that this package
 should only depend on haskell-platform packages and conduit.

Package: libghc-conduit-extra-dev
Architecture: any
Depends: ${haskell:Depends}, ${misc:Depends}, ${shlibs:Depends}
Recommends: ${haskell:Recommends}
Suggests: ${haskell:Suggests}
Provides: ${haskell:Provides}
Description: ${haskell:ShortDescription}${haskell:ShortBlurb}
 ${haskell:LongDescription}
 .
 ${haskell:Blurb}

Package: libghc-conduit-extra-prof
Architecture: any
Depends: ${haskell:Depends}, ${misc:Depends}
Recommends: ${haskell:Recommends}
Suggests: ${haskell:Suggests}
Provides: ${haskell:Provides}
Description: ${haskell:ShortDescription}${haskell:ShortBlurb}
 ${haskell:LongDescription}
 .
 ${haskell:Blurb}

Package: libghc-conduit-extra-doc
Architecture: all
Section: doc
Depends: ${haskell:Depends}, ${misc:Depends}
Recommends: ${haskell:Recommends}
Suggests: ${haskell:Suggests}
Description: ${haskell:ShortDescription}${haskell:ShortBlurb}
 ${haskell:LongDescription}
 .
 ${haskell:Blurb}
