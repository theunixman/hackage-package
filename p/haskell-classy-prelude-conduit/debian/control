Source: haskell-classy-prelude-conduit
Maintainer: Debian Haskell Group <pkg-haskell-maintainers@lists.alioth.debian.org>
Uploaders:
 Clint Adams <clint@debian.org>,
Priority: extra
Section: haskell
Build-Depends:
 cdbs,
 debhelper (>= 9),
 ghc (>= 8),
 ghc-prof,
 haskell-devscripts (>= 0.13),
 libghc-classy-prelude-dev (<< 1.0.1),
 libghc-classy-prelude-dev (>= 1.0.0),
 libghc-classy-prelude-prof,
 libghc-conduit-combinators-dev (>= 0.2.8),
 libghc-conduit-combinators-prof,
 libghc-conduit-dev,
 libghc-conduit-dev (<< 1.3),
 libghc-conduit-dev (>= 1.0),
 libghc-conduit-prof,
 libghc-hspec-dev,
 libghc-monad-control-dev,
 libghc-monad-control-prof,
 libghc-quickcheck2-dev,
 libghc-resourcet-dev,
 libghc-resourcet-prof,
 libghc-void-dev,
 libghc-void-prof,
Build-Depends-Indep:
 ghc-doc,
 libghc-classy-prelude-doc,
 libghc-conduit-combinators-doc,
 libghc-conduit-doc,
 libghc-monad-control-doc,
 libghc-resourcet-doc,
 libghc-void-doc,
Standards-Version: 3.9.8
Homepage: https://github.com/snoyberg/classy-prelude
Vcs-Browser: https://anonscm.debian.org/cgit/pkg-haskell/DHG_packages.git/tree/p/haskell-classy-prelude-conduit
Vcs-Git: https://anonscm.debian.org/cgit/pkg-haskell/DHG_packages.git

Package: libghc-classy-prelude-conduit-dev
Architecture: any
Depends:
 ${haskell:Depends},
 ${misc:Depends},
 ${shlibs:Depends},
Recommends:
 ${haskell:Recommends},
Suggests:
 ${haskell:Suggests},
Provides:
 ${haskell:Provides},
Description: conduit instances for classy-prelude
 classy-prelude-conduit contains conduit instances for classy-prelude.
 .
 This package provides a library for the Haskell programming language.
 See http://www.haskell.org/ for more information on Haskell.

Package: libghc-classy-prelude-conduit-prof
Architecture: any
Depends:
 ${haskell:Depends},
 ${misc:Depends},
Recommends:
 ${haskell:Recommends},
Suggests:
 ${haskell:Suggests},
Provides:
 ${haskell:Provides},
Description: conduit instances for classy-prelude; profiling libraries
 classy-prelude-conduit contains conduit instances for classy-prelude.
 .
 This package provides a library for the Haskell programming language, compiled
 for profiling.  See http://www.haskell.org/ for more information on Haskell.

Package: libghc-classy-prelude-conduit-doc
Architecture: all
Section: doc
Depends:
 ${haskell:Depends},
 ${misc:Depends},
Recommends:
 ${haskell:Recommends},
Suggests:
 ${haskell:Suggests},
Description: conduit instances for classy-prelude; documentation
 classy-prelude-conduit contains conduit instances for classy-prelude.
 .
 This package provides the documentation for a library for the Haskell
 programming language.
 See http://www.haskell.org/ for more information on Haskell.
