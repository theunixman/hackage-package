Source: bnfc
Maintainer: Debian Haskell Group <pkg-haskell-maintainers@lists.alioth.debian.org>
Uploaders:
 Joachim Breitner <nomeata@debian.org>,
 Dmitry Bogatov <KAction@gnu.org>,
Priority: extra
Section: haskell
Build-Depends:
 alex,
 cdbs,
 debhelper (>= 9),
 ghc (>= 8),
 ghc-prof,
 happy,
 haskell-devscripts (>= 0.13),
 libghc-mtl-dev,
Standards-Version: 3.9.8
Homepage: http://bnfc.digitalgrammars.com/
Vcs-Browser: https://anonscm.debian.org/cgit/pkg-haskell/DHG_packages.git/tree/p/bnfc
Vcs-Git: https://anonscm.debian.org/git/pkg-haskell/DHG_packages.git

Package: bnfc
Architecture: any
Section: devel
Depends:
 ${misc:Depends},
 ${shlibs:Depends},
Description: Compiler front-end generator based on Labelled BNF
 The BNF Converter is a compiler construction tool that generates a
 compiler front-end and a readable syntax description document from a
 Labelled BNF grammar. It was originally written to generate Haskell,
 but it can now also be used for generating Java, C++, and C.
 .
 To process Haskell output, you need the Glorious Glasgow Haskell
 Compiler (virtual package ghc), the Happy parser generator (package
 happy) and the Alex scanner generator (package alex).
 .
 To process Java output, you need a Java compiler and virtual machine,
 the CUP parser generator (package cup) and the JLex scanner generator
 (package jlex).
 .
 To process C or C++ output, you need a C or C++ compiler,
 respectively, the Bison parser generator (package bison) and the flex
 scanner generator (package flex).
 .
 To process the generated documents, you need LaTeX (packages
 tetex-base, tetex-bin, etc.).
