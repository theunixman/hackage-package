Source: haskell-basic-prelude
Maintainer: Debian Haskell Group <pkg-haskell-maintainers@lists.alioth.debian.org>
Uploaders:
 Clint Adams <clint@debian.org>,
 Dmitry Bogatov <KAction@gnu.org>,
Priority: extra
Section: haskell
Build-Depends:
 cdbs,
 debhelper (>= 9),
 ghc (>= 8),
 ghc-prof,
 haskell-devscripts (>= 0.13),
 libghc-hashable-dev,
 libghc-hashable-prof,
 libghc-lifted-base-dev,
 libghc-lifted-base-prof,
 libghc-readargs-dev (<< 1.3),
 libghc-readargs-dev (>= 1.2),
 libghc-readargs-prof,
 libghc-safe-dev,
 libghc-safe-prof,
 libghc-text-dev,
 libghc-text-prof,
 libghc-unordered-containers-dev,
 libghc-unordered-containers-prof,
 libghc-vector-dev,
 libghc-vector-prof,
Build-Depends-Indep:
 ghc-doc,
 libghc-hashable-doc,
 libghc-lifted-base-doc,
 libghc-readargs-doc,
 libghc-safe-doc,
 libghc-text-doc,
 libghc-unordered-containers-doc,
 libghc-vector-doc,
Standards-Version: 3.9.8
Homepage: https://github.com/snoyberg/basic-prelude
Vcs-Browser: https://anonscm.debian.org/cgit/pkg-haskell/DHG_packages.git/tree/p/haskell-basic-prelude
Vcs-Git: https://anonscm.debian.org/git/pkg-haskell/DHG_packages.git

Package: libghc-basic-prelude-dev
Architecture: any
Depends:
 ${haskell:Depends},
 ${misc:Depends},
 ${shlibs:Depends},
Recommends:
 ${haskell:Recommends},
Suggests:
 ${haskell:Suggests},
Provides:
 ${haskell:Provides},
Description: enhanced core prelude${haskell:ShortBlurb}
 The premise of basic-prelude is that there are a lot of very commonly
 desired features missing from the standard Prelude, such as commonly
 used operators (<$> and >=>, for instance) and imports for common
 datatypes (e.g., ByteString and Vector). At the same time, there are
 lots of other components which are more debatable, such as providing
 polymorphic versions of common functions.
 .
 So basic-prelude is intended to give a common foundation for a number
 of alternate preludes. The package provides two modules: CorePrelude
 provides the common ground for other preludes to build on top of, while
 BasicPrelude exports CorePrelude together with commonly used list
 functions to provide a drop-in replacement for the standard Prelude.
 .
 Users wishing to have an improved Prelude can use BasicPrelude.
 Developers wishing to create a new prelude should use CorePrelude.
 .
 ${haskell:Blurb}

Package: libghc-basic-prelude-prof
Architecture: any
Depends:
 ${haskell:Depends},
 ${misc:Depends},
Recommends:
 ${haskell:Recommends},
Suggests:
 ${haskell:Suggests},
Provides:
 ${haskell:Provides},
Description: enhanced core prelude${haskell:ShortBlurb}
 The premise of basic-prelude is that there are a lot of very commonly
 desired features missing from the standard Prelude, such as commonly
 used operators (<$> and >=>, for instance) and imports for common
 datatypes (e.g., ByteString and Vector). At the same time, there are
 lots of other components which are more debatable, such as providing
 polymorphic versions of common functions.
 .
 So basic-prelude is intended to give a common foundation for a number
 of alternate preludes. The package provides two modules: CorePrelude
 provides the common ground for other preludes to build on top of, while
 BasicPrelude exports CorePrelude together with commonly used list
 functions to provide a drop-in replacement for the standard Prelude.
 .
 Users wishing to have an improved Prelude can use BasicPrelude.
 Developers wishing to create a new prelude should use CorePrelude.
 .
 ${haskell:Blurb}

Package: libghc-basic-prelude-doc
Architecture: all
Section: doc
Depends:
 ${haskell:Depends},
 ${misc:Depends},
Recommends:
 ${haskell:Recommends},
Suggests:
 ${haskell:Suggests},
Description: enhanced core prelude${haskell:ShortBlurb}
 The premise of basic-prelude is that there are a lot of very commonly
 desired features missing from the standard Prelude, such as commonly
 used operators (<$> and >=>, for instance) and imports for common
 datatypes (e.g., ByteString and Vector). At the same time, there are
 lots of other components which are more debatable, such as providing
 polymorphic versions of common functions.
 .
 So basic-prelude is intended to give a common foundation for a number
 of alternate preludes. The package provides two modules: CorePrelude
 provides the common ground for other preludes to build on top of, while
 BasicPrelude exports CorePrelude together with commonly used list
 functions to provide a drop-in replacement for the standard Prelude.
 .
 Users wishing to have an improved Prelude can use BasicPrelude.
 Developers wishing to create a new prelude should use CorePrelude.
 .
 ${haskell:Blurb}
