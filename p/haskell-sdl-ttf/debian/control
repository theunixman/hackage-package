Source: haskell-sdl-ttf
Maintainer: Debian Haskell Group <pkg-haskell-maintainers@lists.alioth.debian.org>
Uploaders:
 Miriam Ruiz <little_miry@yahoo.es>,
Priority: extra
Section: haskell
Build-Depends:
 alex,
 c2hs,
 cdbs,
 cpphs,
 debhelper (>= 9),
 ghc (>= 8),
 ghc-prof,
 haskell-devscripts (>= 0.13),
 libghc-sdl-dev,
 libghc-sdl-prof,
 libgl1-mesa-dev | libgl-dev,
 libglu1-mesa-dev | libglu-dev,
 libsdl-ttf2.0-dev,
 libsdl1.2-dev,
 libx11-dev,
 pkg-config,
Build-Depends-Indep:
 ghc-doc,
Standards-Version: 3.9.8
Homepage: http://hackage.haskell.org/package/SDL-ttf
Vcs-Browser: https://anonscm.debian.org/cgit/pkg-haskell/DHG_packages.git/tree/p/haskell-sdl-ttf
Vcs-Git: https://anonscm.debian.org/git/pkg-haskell/DHG_packages.git

Package: libghc-sdl-ttf-dev
Architecture: any
Depends:
 libghc-sdl-dev,
 libgl1-mesa-dev | libgl-dev,
 libglu1-mesa-dev | libglu-dev,
 libsdl-ttf2.0-dev,
 libsdl1.2-dev,
 libx11-dev,
 ${haskell:Depends},
 ${misc:Depends},
 ${shlibs:Depends},
Suggests:
 libghc-sdl-ttf-doc,
 libghc-sdl-ttf-prof,
Description: Haskell SDL TTF binding for GHC
 This package provides the SDL TTF library bindings for the Haskell
 programming language. SDL TTF allows you to use TrueType fonts in SDL
 applications.

Package: libghc-sdl-ttf-prof
Architecture: any
Depends:
 libghc-sdl-prof,
 libghc-sdl-ttf-dev (= ${binary:Version}),
 ${haskell:Depends},
 ${misc:Depends},
 ${shlibs:Depends},
Suggests:
 libghc-sdl-ttf-doc,
Description: Haskell SDL TTF binding for GHC - profiling libraries
 This package provides the SDL TTF library bindings for the Haskell
 programming language, compiled for profiling. SDL TTF allows you
 to use TrueType fonts in SDL applications.

Package: libghc-sdl-ttf-doc
Architecture: all
Section: doc
Depends:
 ${haskell:Depends},
 ${misc:Depends},
 ${shlibs:Depends},
Recommends:
 ghc-doc,
Suggests:
 libghc-sdl-ttf-dev,
 libghc-sdl-ttf-prof,
Description: Haskell SDL TTF binding for GHC - documentation
 This package provides the documentation for the SDL TTF library bindings
 for the Haskell programming language. SDL TTF allows you to use TrueType
 fonts in SDL applications.
