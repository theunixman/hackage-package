Source: haskell-doctest
Maintainer: Debian Haskell Group <pkg-haskell-maintainers@lists.alioth.debian.org>
Uploaders: Clint Adams <clint@debian.org>
Priority: extra
Section: haskell
Build-Depends: debhelper (>= 9),
 haskell-devscripts (>= 0.13),
 cdbs,
 ghc (>= 8.0),
 ghc-prof,
 libghc-base-compat-dev (>= 0.4.2),
 libghc-base-compat-prof,
 libghc-ghc-paths-dev (>= 0.1.0.9),
 libghc-ghc-paths-prof,
 libghc-syb-dev (>= 0.3),
 libghc-syb-prof,
Build-Depends-Indep: ghc-doc,
 libghc-base-compat-doc,
 libghc-ghc-paths-doc,
 libghc-syb-doc,
Standards-Version: 3.9.8
Homepage: https://github.com/sol/doctest-haskell
Vcs-Git: https://anonscm.debian.org/git/pkg-haskell/DHG_packages.git
Vcs-Browser: https://anonscm.debian.org/cgit/pkg-haskell/DHG_packages.git/tree/p/haskell-doctest

Package: libghc-doctest-dev
Architecture: any
Depends: ${shlibs:Depends},
 ${haskell:Depends},
 ${misc:Depends},
Recommends: ${haskell:Recommends},
Suggests: ${haskell:Suggests},
Provides: ${haskell:Provides},
Description: test interactive Haskell examples
 The doctest program checks examples in source code comments.
 It is modeled after doctest for Python
 (<http://docs.python.org/library/doctest.html>).
 .
 Documentation is at
 <https://github.com/sol/doctest-haskell#readme>.
 .
 This package contains the normal library files.

Package: libghc-doctest-prof
Architecture: any
Depends: ${haskell:Depends},
 ${misc:Depends},
Recommends: ${haskell:Recommends},
Suggests: ${haskell:Suggests},
Provides: ${haskell:Provides},
Description: test interactive Haskell examples; profiling libraries
 The doctest program checks examples in source code comments.
 It is modeled after doctest for Python
 (<http://docs.python.org/library/doctest.html>).
 .
 Documentation is at
 <https://github.com/sol/doctest-haskell#readme>.
 .
 This package contains the libraries compiled with profiling enabled.

Package: libghc-doctest-doc
Architecture: all
Section: doc
Depends: ${haskell:Depends},
 ${misc:Depends},
Recommends: ${haskell:Recommends},
Suggests: ${haskell:Suggests},
Description: test interactive Haskell examples; documentation
 The doctest program checks examples in source code comments.
 It is modeled after doctest for Python
 (<http://docs.python.org/library/doctest.html>).
 .
 Documentation is at
 <https://github.com/sol/doctest-haskell#readme>.
 .
 This package contains the documentation files.

Package: doctest
Architecture: any
Section: devel
Depends: ${shlibs:Depends},
 ${haskell:Depends},
 ${misc:Depends},
Description: test interactive Haskell examples; executable
 The doctest program checks examples in source code comments.
 It is modeled after doctest for Python
 (<http://docs.python.org/library/doctest.html>).
 .
 Documentation is at
 <https://github.com/sol/doctest-haskell#readme>.
 .
 This package contains the doctest executable.
