Source: hdbc-postgresql
Maintainer: Debian Haskell Group <pkg-haskell-maintainers@lists.alioth.debian.org>
Uploaders:
 John Goerzen <jgoerzen@complete.org>,
Priority: extra
Section: haskell
Build-Depends:
 cdbs,
 cpphs,
 debhelper (>= 9),
 ghc (>= 8),
 ghc-prof,
 haddock,
 haskell-devscripts (>= 0.13),
 hscolour,
 libghc-convertible-dev,
 libghc-convertible-prof,
 libghc-hdbc-dev (>= 2.2.0),
 libghc-hdbc-prof (>= 2.2.0),
 libghc-mtl-dev,
 libghc-mtl-prof,
 libghc-parsec3-dev,
 libghc-parsec3-prof,
 libghc-utf8-string-dev,
 libghc-utf8-string-prof,
 libpq-dev (>= 8.1.1),
Build-Depends-Indep:
 ghc-doc,
 libghc-convertible-doc,
 libghc-hdbc-doc,
 libghc-mtl-doc,
 libghc-parsec3-doc,
 libghc-utf8-string-doc,
Standards-Version: 3.9.8
Homepage: http://hackage.haskell.org/package/HDBC-postgresql
Vcs-Browser: https://anonscm.debian.org/cgit/pkg-haskell/DHG_packages.git/tree/p/hdbc-postgresql
Vcs-Git: https://anonscm.debian.org/git/pkg-haskell/DHG_packages.git

Package: libghc-hdbc-postgresql-dev
Architecture: any
Depends:
 libpq-dev (>= 8.1.1),
 ${haskell:Depends},
 ${misc:Depends},
 ${shlibs:Depends},
Recommends:
 ${haskell:Recommends},
Suggests:
 ${haskell:Suggests},
Provides:
 ${haskell:Provides},
Description: PostgreSQL HDBC (Haskell Database Connectivity) Driver for GHC
 HDBC provides an abstraction layer between Haskell programs and SQL
 relational databases. This lets you write database code once, in
 Haskell, and have it work with any number of backend SQL databases.
 .
 This package provides the PostgreSQL database driver for HDBC under GHC.

Package: libghc-hdbc-postgresql-prof
Architecture: any
Depends:
 ${haskell:Depends},
 ${misc:Depends},
 ${shlibs:Depends},
Recommends:
 ${haskell:Recommends},
Suggests:
 ${haskell:Suggests},
Provides:
 ${haskell:Provides},
Description: PostgreSQL HDBC Driver for GHC; profiling libraries
 HDBC provides an abstraction layer between Haskell programs and SQL
 relational databases. This lets you write database code once, in
 Haskell, and have it work with any number of backend SQL databases.
 .
 This package provides the PostgreSQL database driver for HDBC under GHC
 compiled for profiling.

Package: libghc-hdbc-postgresql-doc
Architecture: all
Section: doc
Depends:
 ${haskell:Depends},
 ${misc:Depends},
Recommends:
 ${haskell:Recommends},
Suggests:
 ${haskell:Suggests},
Conflicts:
 haskell-hdbc-postgresql-doc (<< 2.2.3.1-3),
Provides:
 haskell-hdbc-postgresql-doc,
 ${haskell:Provides},
Replaces:
 haskell-hdbc-postgresql-doc (<< 2.2.3.1-3),
Description: PostgreSQL HDBC (Haskell Database Connectivity) documentation
 HDBC provides an abstraction layer between Haskell programs and SQL
 relational databases. This lets you write database code once, in
 Haskell, and have it work with any number of backend SQL databases.
