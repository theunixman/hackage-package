Source: haskell-digest
Maintainer: Debian Haskell Group <pkg-haskell-maintainers@lists.alioth.debian.org>
Uploaders: Joachim Breitner <nomeata@debian.org>
Priority: extra
Section: haskell
Build-Depends: debhelper (>= 9),
 haskell-devscripts (>= 0.13),
 cdbs,
 ghc (>= 8),
 ghc-prof,
 zlib1g-dev,
Build-Depends-Indep: ghc-doc,
Standards-Version: 3.9.8
Homepage: http://hackage.haskell.org/package/digest
Vcs-Browser: https://anonscm.debian.org/cgit/pkg-haskell/DHG_packages.git/tree/p/haskell-digest
Vcs-Git: https://anonscm.debian.org/git/pkg-haskell/DHG_packages.git

Package: libghc-digest-dev
Architecture: any
Depends: ${haskell:Depends},
 ${shlibs:Depends},
 ${misc:Depends},
 zlib1g-dev,
Suggests: ${haskell:Suggests},
Provides: ${haskell:Provides},
Description: efficient cryptographic hash implementations for bytestrings${haskell:ShortBlurb}
 This package provides efficient cryptographic hash implementations for strict
 and lazy bytestrings. For now, CRC32 and Adler32 are supported; they are
 implemented as FFI bindings to efficient code from zlib.
 .
 ${haskell:Blurb}

Package: libghc-digest-prof
Architecture: any
Depends: ${haskell:Depends},
 ${shlibs:Depends},
 ${misc:Depends},
Provides: ${haskell:Provides},
Description: efficient cryptographic hash implementations bytestrings${haskell:ShortBlurb}
 This package provides efficient cryptographic hash implementations for strict
 and lazy bytestrings. For now, CRC32 and Adler32 are supported; they are
 implemented as FFI bindings to efficient code from zlib.
 .
 ${haskell:Blurb}

Package: libghc-digest-doc
Architecture: all
Section: doc
Depends: ${haskell:Depends},
 ${misc:Depends},
Recommends: ${haskell:Recommends},
Suggests: ${haskell:Suggests},
Description: efficient cryptographic hash implementations bytestrings${haskell:ShortBlurb}
 This package provides efficient cryptographic hash implementations for strict
 and lazy bytestrings. For now, CRC32 and Adler32 are supported; they are
 implemented as FFI bindings to efficient code from zlib.
 .
 ${haskell:Blurb}
