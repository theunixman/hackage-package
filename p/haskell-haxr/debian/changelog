haskell-haxr (3000.11.2-3) unstable; urgency=medium

  * Upload to unstable as part of GHC 8 transition.

 -- Clint Adams <clint@debian.org>  Thu, 27 Oct 2016 18:34:01 -0400

haskell-haxr (3000.11.2-2) experimental; urgency=medium

  * Temporarily build-depend on ghc 8.

 -- Clint Adams <clint@debian.org>  Mon, 17 Oct 2016 17:07:01 -0400

haskell-haxr (3000.11.2-1) unstable; urgency=medium

  * New upstream release

 -- Clint Adams <clint@debian.org>  Thu, 25 Aug 2016 22:38:18 -0400

haskell-haxr (3000.11.1.6-1) unstable; urgency=medium

  * New upstream release

 -- Clint Adams <clint@debian.org>  Tue, 12 Jul 2016 16:40:07 -0400

haskell-haxr (3000.11.1.5-1) unstable; urgency=medium

  [ Dmitry Bogatov ]
  * Use secure (https) uri in Vcs-Git field in 'debian/control'
  * Bump standards version to 3.9.8 (no changes needed)

  [ Joachim Breitner ]
  * New upstream release

 -- Joachim Breitner <nomeata@debian.org>  Sun, 29 May 2016 12:11:36 +0200

haskell-haxr (3000.11.1.4-1) unstable; urgency=medium

  * New upstream release

 -- Clint Adams <clint@debian.org>  Sat, 09 Apr 2016 15:39:03 -0400

haskell-haxr (3000.11.1.3-1) unstable; urgency=medium

  * New upstream release

 -- Clint Adams <clint@debian.org>  Sun, 13 Mar 2016 20:02:10 -0400

haskell-haxr (3000.11.1.2-1) unstable; urgency=medium

  * New upstream release

 -- Clint Adams <clint@debian.org>  Fri, 04 Dec 2015 00:09:47 -0500

haskell-haxr (3000.11.1.1-3) unstable; urgency=medium

  * Switch Vcs-Git/Vcs-Browser headers to new location.

 -- Clint Adams <clint@debian.org>  Thu, 03 Dec 2015 14:54:32 -0500

haskell-haxr (3000.11.1.1-2) experimental; urgency=medium

  * Depend on old-{locale,time}
  * Bump standards-version to 3.9.6
  * Depend on haskell-devscripts >= 0.10 to ensure that this package
    builds against GHC in experimental

 -- Joachim Breitner <nomeata@debian.org>  Thu, 20 Aug 2015 10:27:47 +0200

haskell-haxr (3000.11.1.1-1) unstable; urgency=medium

  * New upstream release

 -- Joachim Breitner <nomeata@debian.org>  Sat, 15 Aug 2015 13:50:36 +0200

haskell-haxr (3000.10.4.2-1) unstable; urgency=medium

  * New upstream release

 -- Joachim Breitner <nomeata@debian.org>  Wed, 29 Apr 2015 19:59:35 +0200

haskell-haxr (3000.9.0.1-3) experimental; urgency=low

  * Adjust watch file to new hackage layout
  * Depend on haskell-devscripts 0.9, found in experimental

 -- Joachim Breitner <nomeata@debian.org>  Sat, 20 Dec 2014 17:10:47 +0100

haskell-haxr (3000.9.0.1-2) unstable; urgency=low

  * Enable compat level 9

 -- Joachim Breitner <nomeata@debian.org>  Fri, 24 May 2013 12:50:51 +0200

haskell-haxr (3000.9.0.1-1) experimental; urgency=low

  * Depend on haskell-devscripts 0.8.13 to ensure this packages is built
    against experimental
  * New upstream release

 -- Joachim Breitner <nomeata@debian.org>  Thu, 07 Feb 2013 19:30:02 +0100

haskell-haxr (3000.8.5-2) experimental; urgency=low

  [ Giovanni Mascellani ]
  * Team upload.
  * Fix Homepage: link

  [ Joachim Breitner ]
  * Bump standards version, no change

 -- Joachim Breitner <nomeata@debian.org>  Sun, 14 Oct 2012 12:01:01 +0200

haskell-haxr (3000.8.5-1) unstable; urgency=low

  * New upstream release
  * Bump Standards-Version to 3.9.3.
    + debian/control: Update Format URI.

 -- TANIGUCHI Takaki <takaki@debian.org>  Mon, 05 Mar 2012 13:28:08 +0900

haskell-haxr (3000.8.4-1) unstable; urgency=low

  * New upstream release

 -- TANIGUCHI Takaki <takaki@debian.org>  Thu, 11 Aug 2011 21:35:03 +0900

haskell-haxr (3000.8.2-1) unstable; urgency=low

  * New upstream release

 -- TANIGUCHI Takaki <takaki@debian.org>  Tue, 14 Jun 2011 19:35:58 +0900

haskell-haxr (3000.8.1-2) unstable; urgency=low

  * Add missing *-prof build depends, thx to ucko for the report.  (Closes:
    #628182)

 -- Joachim Breitner <nomeata@debian.org>  Sat, 28 May 2011 21:24:52 +0200

haskell-haxr (3000.8.1-1) unstable; urgency=low

  [ Marco Silva ]
  * Use ghc instead of ghc6

  [ TANIGUCHI Takaki ]
  * debian/control: Change Maintainer to Haskell Team.

  [ Joachim Breitner ]
  * New upstream release

  [ Marco Túlio Gontijo e Silva ]
  * control: Bump Standards-Version to 3.9.2.  No changes needed.
  * rules: Remove unneeded atribution of DEB_CABAL_PACKAGE.
  * control: Move -doc build-dependencies to Build-Depends-Indep.
  * control: Add fields Vcs-Darcs and Vcs-Browser.
  * control: Remove references to GHC 6.
  * control: Update Build-Dependencies for new version.
  * Use template-debian watch file.

  [ Joachim Breitner ]
  * Depend on ghci, as it uses Template Haskell
  * Source format 3.0 (quilt)

 -- Joachim Breitner <nomeata@debian.org>  Fri, 27 May 2011 16:40:35 +0200

haskell-haxr (3000.2.1-2) unstable; urgency=low

  * debian/control: Remove hscolour from Build-Depends:, since it's now a
    Depends: of haskell-devscripts.
  * debian/control: Remove haddock from Build-Depends:, since it's now a
    Depends: of haskell-devscripts.
  * Bump haskell-devscripts dependency to 0.7.
  * debian/control: Bump Standards-Version: to 3.8.4, no changes needed.

 -- TANIGUCHI Takaki <takaki@debian.org>  Fri, 26 Feb 2010 17:07:30 +0900

haskell-haxr (3000.2.1-1) unstable; urgency=low

  * Initial release (Closes: #458367)

 -- TANIGUCHI Takaki <takaki@debian.org>  Thu, 12 Nov 2009 21:44:08 +0900
