Source: haskell-generic-trie
Maintainer: Debian Haskell Group <pkg-haskell-maintainers@lists.alioth.debian.org>
Uploaders: Joachim Breitner <nomeata@debian.org>
Priority: extra
Section: haskell
Build-Depends: debhelper (>= 9),
 haskell-devscripts (>= 0.13),
 cdbs,
 ghc (>= 8),
 ghc-prof,
Build-Depends-Indep: ghc-doc,
Standards-Version: 3.9.8
Homepage: http://github.com/glguy/tries
Vcs-Browser: https://anonscm.debian.org/cgit/pkg-haskell/DHG_packages.git/tree/p/haskell-generic-trie
Vcs-Git: https://anonscm.debian.org/git/pkg-haskell/DHG_packages.git
X-Description: a map, where the keys may be complex structured data
 This type implements maps where the keys are themselves complex structured
 data. For example, the keys may be the abstract syntax trees for a
 programming language. The map is implemented as a trie, so common parts of
 the keys will be shared in the representation. The library provides a generic
 implementation of the data structure, so values of types that have support for
 'Generic' may be automatically used as keys in the map.

Package: libghc-generic-trie-dev
Architecture: any
Depends: ${haskell:Depends},
 ${misc:Depends},
 ${shlibs:Depends},
Recommends: ${haskell:Recommends},
Suggests: ${haskell:Suggests},
Conflicts: ${haskell:Conflicts},
Provides: ${haskell:Provides},
Description: ${haskell:ShortDescription}${haskell:ShortBlurb}
 ${haskell:LongDescription}
 .
 ${haskell:Blurb}

Package: libghc-generic-trie-prof
Architecture: any
Depends: ${haskell:Depends},
 ${misc:Depends},
Recommends: ${haskell:Recommends},
Suggests: ${haskell:Suggests},
Conflicts: ${haskell:Conflicts},
Provides: ${haskell:Provides},
Description: ${haskell:ShortDescription}${haskell:ShortBlurb}
 ${haskell:LongDescription}
 .
 ${haskell:Blurb}

Package: libghc-generic-trie-doc
Architecture: all
Section: doc
Depends: ${haskell:Depends},
 ${misc:Depends},
Recommends: ${haskell:Recommends},
Suggests: ${haskell:Suggests},
Conflicts: ${haskell:Conflicts},
Description: ${haskell:ShortDescription}${haskell:ShortBlurb}
 ${haskell:LongDescription}
 .
 ${haskell:Blurb}
