Source: haskell-mtl
Maintainer: Debian Haskell Group <pkg-haskell-maintainers@lists.alioth.debian.org>
Uploaders: Joachim Breitner <nomeata@debian.org>
Priority: extra
Section: haskell
Build-Depends: debhelper (>= 9),
 haskell-devscripts (>= 0.13),
 cdbs,
 ghc (>= 8),
 ghc-prof,
Build-Depends-Indep: ghc-doc,
Standards-Version: 3.9.8
Homepage: http://github.com/ekmett/mtl
Vcs-Browser: https://anonscm.debian.org/cgit/pkg-haskell/DHG_packages.git/tree/p/haskell-mtl
Vcs-Git: https://anonscm.debian.org/cgit/pkg-haskell/DHG_packages.git

Package: libghc-mtl-dev
Architecture: any
Depends: ${haskell:Depends},
 ${misc:Depends},
 ${shlibs:Depends},
Recommends: ${haskell:Recommends},
Suggests: ${haskell:Suggests},
Provides: ${haskell:Provides},
Description: Haskell monad transformer library for GHC${haskell:ShortBlurb}
 MTL is a monad transformer library, inspired by the paper "Functional
 Programming with Overloading and Higher-Order Polymorphism",
 by Mark P Jones (<http://www.cse.ogi.edu/~mpj/>), Advanced School
 of Functional Programming, 1995.
 .
 ${haskell:Blurb}

Package: libghc-mtl-prof
Architecture: any
Depends: ${haskell:Depends},
 ${misc:Depends},
Recommends: ${haskell:Recommends},
Suggests: ${haskell:Suggests},
Provides: ${haskell:Provides},
Description: Haskell monad transformer library for GHC${haskell:ShortBlurb}
 MTL is a monad transformer library, inspired by the paper "Functional
 Programming with Overloading and Higher-Order Polymorphism",
 by Mark P Jones (<http://www.cse.ogi.edu/~mpj/>), Advanced School
 of Functional Programming, 1995.
 .
 ${haskell:Blurb}

Package: libghc-mtl-doc
Architecture: all
Section: doc
Depends: ${haskell:Depends},
 ${misc:Depends},
Recommends: ${haskell:Recommends},
Suggests: ${haskell:Suggests},
Description: Haskell monad transformer library for GHC${haskell:ShortBlurb}
 MTL is a monad transformer library, inspired by the paper "Functional
 Programming with Overloading and Higher-Order Polymorphism",
 by Mark P Jones (<http://www.cse.ogi.edu/~mpj/>), Advanced School
 of Functional Programming, 1995.
 .
 ${haskell:Blurb}
