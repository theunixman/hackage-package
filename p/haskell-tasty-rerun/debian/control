Source: haskell-tasty-rerun
Maintainer: Debian Haskell Group <pkg-haskell-maintainers@lists.alioth.debian.org>
Uploaders:
 Joey Hess <joeyh@debian.org>,
Priority: extra
Section: haskell
Build-Depends:
 cdbs,
 debhelper (>= 9),
 ghc (>= 8),
 ghc-prof,
 haskell-devscripts (>= 0.13),
 libghc-mtl-dev (>= 2.1.2),
 libghc-mtl-prof,
 libghc-optparse-applicative-dev (>= 0.6),
 libghc-optparse-applicative-prof,
 libghc-reducers-dev (>= 3.10.1),
 libghc-reducers-prof,
 libghc-split-dev (<< 0.3),
 libghc-split-dev (>= 0.1),
 libghc-split-prof,
 libghc-stm-dev (>= 2.4.2),
 libghc-stm-prof,
 libghc-tagged-dev (<< 0.9),
 libghc-tagged-dev (>= 0.7),
 libghc-tagged-prof,
 libghc-tasty-dev (<< 0.12),
 libghc-tasty-dev (>= 0.10),
 libghc-tasty-prof,
Build-Depends-Indep:
 ghc-doc,
 libghc-mtl-doc,
 libghc-optparse-applicative-doc,
 libghc-reducers-doc,
 libghc-split-doc,
 libghc-stm-doc,
 libghc-tagged-doc,
 libghc-tasty-doc,
Standards-Version: 3.9.8
Homepage: http://github.com/ocharles/tasty-rerun
Vcs-Browser: https://anonscm.debian.org/cgit/pkg-haskell/DHG_packages.git/tree/p/haskell-tasty-rerun
Vcs-Git: https://anonscm.debian.org/git/pkg-haskell/DHG_packages.git

Package: libghc-tasty-rerun-dev
Architecture: any
Depends:
 ${haskell:Depends},
 ${misc:Depends},
 ${shlibs:Depends},
Suggests:
 ${haskell:Suggests},
Provides:
 ${haskell:Provides},
Description: Haskell testing framework, rerun support${haskell:ShortBlurb}
 This ingredient adds the ability to run tests by first filtering the
 test tree based on the result of a previous test run.
 For example, you can use this to run only those tests that failed
 in the last run, or to run only tests that have been added since
 tests were last ran.
 .
 ${haskell:Blurb}

Package: libghc-tasty-rerun-prof
Architecture: any
Depends:
 ${haskell:Depends},
 ${misc:Depends},
Suggests:
 ${haskell:Suggests},
Provides:
 ${haskell:Provides},
Description: Haskell testing framework, rerun support${haskell:ShortBlurb}
 This ingredient adds the ability to run tests by first filtering the
 test tree based on the result of a previous test run.
 For example, you can use this to run only those tests that failed
 in the last run, or to run only tests that have been added since
 tests were last ran.
 .
 ${haskell:Blurb}

Package: libghc-tasty-rerun-doc
Architecture: all
Section: doc
Depends:
 ${haskell:Depends},
 ${misc:Depends},
Recommends:
 ${haskell:Recommends},
Suggests:
 ${haskell:Suggests},
Description: Haskell testing framework, rereun support${haskell:ShortBlurb}
 This ingredient adds the ability to run tests by first filtering the
 test tree based on the result of a previous test run.
 For example, you can use this to run only those tests that failed
 in the last run, or to run only tests that have been added since
 tests were last ran.
 .
 ${haskell:Blurb}
